
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function calcular()
{
    let precio = document.getElementById('idPrecio').value;
    let porIni = document.getElementById('idPorcentaje').value;
    let plazo = document.getElementById('idPlazo').value;

    let pagoIni = precio*(porIni/100);
    let totalFinanciar = precio-pagoIni;
    let mensualidad = totalFinanciar/plazo;

    document.getElementById('idPagoInicial').value = pagoIni;
    document.getElementById('idTotalFin').value = totalFinanciar;
    document.getElementById('idPagoMensual').value = mensualidad;
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click',function limpiar()
{
    document.getElementById('idPrecio').value = 0;
    document.getElementById('idPorcentaje').value = 0;
    document.getElementById('idPlazo').value = 0;
    document.getElementById('idPagoInicial').value = 0;
    document.getElementById('idTotalFin').value = 0;
    document.getElementById('idPagoMensual').value = 0;
    document.getElementById('descripcion').value = 0;
});